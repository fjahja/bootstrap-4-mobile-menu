# Project Title

A mobile menu for responsive websites inspired by the main bottom navigation on IOS App Store.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* jQuery 3.2.1
* Bootstrap 4.0.0
* Fontawesome Icons
